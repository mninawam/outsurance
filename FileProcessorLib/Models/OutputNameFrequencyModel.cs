﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileProcessorLib.Models
{
    [DelimitedRecord(",")]
    class OutputNameFrequencyModel
    {
        public string Name { get; set; }
        public int Count { get; set; }

    }
}
