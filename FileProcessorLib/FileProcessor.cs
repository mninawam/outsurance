﻿using System;
using System.IO;
using FileProcessorLib.Models;
using FileHelpers;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace FileProcessorLib
{
    public class FileProcessor
    {
        private string _sInputFile = "";
        private string _sOutputFolder = "";
        private StreamReader _stFileReader = null;
        FileHelperEngine<InputFileModel> engineInput = new FileHelperEngine<InputFileModel>(Encoding.UTF8);

        public FileProcessor(string sInputFile, string sOutputFolder)
        {
            _sInputFile = sInputFile;
            _sOutputFolder = sOutputFolder;
        }

        #region "Public Methods"

        public bool ProcessFile(out string sResultMessage)
        {

            bool bResult = false
                ;
            sResultMessage = "";
            try
            {

                bResult = ProcessNameFrequency();
                bResult = ProcessAddress();

            }
            catch(Exception ex)
            {
                sResultMessage = ex.Message;
            }

            return bResult;
        }

        #endregion

        #region "Private Methods"

        private bool ProcessNameFrequency()
        {

            bool bResult = false;
            OutputNameFrequencyModel outputFreq = null;
            List<OutputNameFrequencyModel> outputFreqList = new List<OutputNameFrequencyModel>();

            var engineOutputCount = new FileHelperEngine<OutputNameFrequencyModel>(Encoding.UTF8);

            InputFileModel[] fileRecords = engineInput.ReadFile(_sInputFile);

            //Get the Count of FirstName
            var resultsFirstName = from p in fileRecords
                                   group p by p.FirstName into g
                                   select new { Name = g.Key, Count = g.ToList().Count };
            //Get the Count of LastName
            var resultsLastName = from p in fileRecords
                                  group p by p.LastName into g
                                  select new { Name = g.Key, Count = g.ToList().Count };
            //Union the 2 results into one Object
            var resultCombined = from p in resultsFirstName.Union(resultsLastName)
                                 group p by p.Name into g
                                 select new { Name = g.Key, Sum = g.Sum(e => e.Count) };

            if (resultCombined != null)
            {
                //Sort the Results, as per the requirement, i.e
                //Sort by Name ascending first, and then by Count descending
                resultCombined = resultCombined.OrderBy(e => e.Name).OrderByDescending(e => e.Sum);
                foreach (var res in resultCombined)
                {

                    //Try and Skip the Column names.
                    if (res.Name != "FirstName" && res.Name != "LastName")
                    {
                        outputFreq = new OutputNameFrequencyModel();
                        outputFreq.Name = res.Name;
                        outputFreq.Count = res.Sum;

                        outputFreqList.Add(outputFreq);
                    }
                }

                //Write the Result into a Output file
                engineOutputCount.WriteFile(Path.Combine(_sOutputFolder, "NameFrequencyCount.csv"), outputFreqList);

                bResult = true;
            }

            return bResult;

        }

        private bool ProcessAddress()
        {

            bool bResult = false;
            OutputAddressSortModel outputAddress = null;
            List<OutputAddressSortModel> outputAddressList = new List<OutputAddressSortModel>();

            var engineOutputAddress = new FileHelperEngine<OutputAddressSortModel>(Encoding.UTF8);
            InputFileModel[] fileRecords = engineInput.ReadFile(_sInputFile);

            var resultsAddress = from p in fileRecords
                                   select new { NewAddress = new string(p.Address.Where(c => !char.IsDigit(c)).ToArray()), OriginalAddress = p.Address};

            if (resultsAddress != null)
            {
                //Sort the Results, as per the requirement
                resultsAddress = resultsAddress.OrderBy(e => e.NewAddress);
                foreach (var res in resultsAddress)
                {
                    
                    //Try and skip the Column names.
                    if (res.OriginalAddress != "Address")
                    {
                        outputAddress = new OutputAddressSortModel();
                        outputAddress.Address = res.OriginalAddress;

                        outputAddressList.Add(outputAddress);
                    }
                }

                engineOutputAddress.WriteFile(Path.Combine(_sOutputFolder, "AddressSort.csv"), outputAddressList);

                bResult = true;
            }

            return bResult;

        }

        #endregion

    }

}
