﻿using System;
using System.Windows.Forms;
using System.IO;
using FileProcessorLib;

namespace FileProcessing
{
    public partial class FormFileProcessing : Form
    {

        #region "Local Variables"

        private string _sInputFile = "";
        private string _sOutputFolder = "";

        #endregion

        public FormFileProcessing()
        {
            InitializeComponent();
        }

        #region "Local Event Handlers"

        private void buttonProcessFile_Click(object sender, EventArgs e)
        {

            bool bResult = false;
            string sResultMessage = "";

            //First validate the Form before processing the file
            if (ValidateForm())
            {
                //Create the instance of the FileProcessor class, and call the ProcessFile method to process the file.
                FileProcessor fp = new FileProcessor(_sInputFile, _sOutputFolder);
                bResult = fp.ProcessFile(out sResultMessage);

                //If the Result from the ProceFile is false, that means there is an error, display the error on the Error Label, or displaye Success.
                if (!bResult)
                {
                    labelResult.Text = sResultMessage;
                }
                else
                {
                    labelResult.Text = "Success: InputFile processed.";
                }
            }

        }

        private void buttonBrowseInputFile_Click(object sender, EventArgs e)
        {

            //Open the Dialog to read the CSV file.
            //Make default folder the Application's path
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV files (*.csv)|*.csv";
            openFileDialog.InitialDirectory = Application.StartupPath;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _sInputFile = openFileDialog.FileName;
                textBoxInputFile.Text = _sInputFile;
            }

        }

        private void buttonBrowseOutputFolder_Click(object sender, EventArgs e)
        {

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    _sOutputFolder = fbd.SelectedPath;
                    textBoxOutputFolder.Text = _sOutputFolder;
                }
            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        #endregion

        #region "Private Methods"

        private void ClearForm()
        {
            labelResult.Text = "";
            textBoxInputFile.Text = "";
            textBoxOutputFolder.Text = "";
        }

        private bool ValidateForm()
        {
            bool bResult = true;

            if (textBoxInputFile.Text.Trim().ToString() == "" || _sInputFile.Trim().ToString() == "")
            {
                MessageBox.Show("The Input File cannot be Empty", "File Processor", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                bResult = false;
            }
            if (textBoxOutputFolder.Text.Trim().ToString() == "" || _sOutputFolder.Trim().ToString() == "")
            {
                MessageBox.Show("The Output Folder cannot be Empty", "File Processor", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                bResult = false;
            }

            return bResult;
        }

        #endregion

    }

}

