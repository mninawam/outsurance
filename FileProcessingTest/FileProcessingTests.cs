﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileProcessorLib;

namespace FileProcessingTest
{
    [TestClass]
    public class FileProcessingTests
    {
        [TestMethod]
        public void TestProcessFile()
        {

            // arrange  
            string _sInputFile = "C:\\Test.csv";
            string _sOutputFolder = "";
            string sResultMessage = "";
            bool bBxpected = true;

            // Do the Test Action  
            FileProcessor fp = new FileProcessor(_sInputFile, _sOutputFolder);

            // assert  
            bool bActual = fp.ProcessFile(out sResultMessage);

            Assert.AreEqual(bBxpected, bActual, sResultMessage);
        }
    }
}
